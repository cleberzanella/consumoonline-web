package com.analytica.consumoonline.controller;

import com.analytica.consumoonline.service.CelescService;
import com.analytica.consumoonline.service.TimService;
import spark.Request;
import spark.Response;

import java.util.List;

import static spark.Spark.halt;

/**
 * Created by cleberzanella on 15/08/17.
 */
public class TimController {

    public static List<TimService.ConsumptionItem> getConsumption(Request request, Response response) {

        if(!request.queryParams().contains("loginNumber") || !request.queryParams().contains("password")){
            halt(400, "Query parameters loginNumber and password are need.");
        }

        String loginNumber = request.queryParams("loginNumber");
        Integer password = Integer.parseInt(request.queryParams("password"));

        TimService service = new TimService();
        return service.getActualDebits(loginNumber, password);
    }

}
