package com.analytica.consumoonline.controller;

import com.analytica.consumoonline.service.CelescService;
import spark.Request;
import spark.Response;
import static spark.Spark.halt;

import java.util.List;

/**
 * Created by cleberzanella on 15/08/17.
 */
public class CelescController {

    public static List<CelescService.Debit> getDebits(Request request, Response response) {

        if(!request.queryParams().contains("cpfCnpj") || !request.queryParams().contains("consumptionUnit")){
            halt(400, "Query parameters cpfCnpj and consumptionUnit are need.");
        }

        String cpfCnpj = request.queryParams("cpfCnpj");
        Integer consumptionUnit = Integer.parseInt(request.queryParams("consumptionUnit"));

        CelescService service = new CelescService();
        return service.getActualDebits(cpfCnpj, consumptionUnit);
    }

}
