package com.analytica.consumoonline.controllerprovider;

import com.analytica.consumoonline.controller.CelescController;

import spark.Service;

import static com.analytica.consumoonline.core.serialization.ResponseUtil.json;

/**
 * Created by cleberzanella on 15/08/17.
 */
public class CelescControllerProvider {

    private Service service;

    public CelescControllerProvider(Service service) {
        this.service = service;
    }

    public void loadRoutes(){

        service.path("/celesc", () -> {

            service.get("/debits", CelescController::getDebits, json());

        });

    }

}
