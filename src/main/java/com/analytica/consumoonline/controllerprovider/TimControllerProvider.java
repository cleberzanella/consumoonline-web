package com.analytica.consumoonline.controllerprovider;

import com.analytica.consumoonline.controller.CelescController;
import com.analytica.consumoonline.controller.TimController;
import spark.Service;

import static com.analytica.consumoonline.core.serialization.ResponseUtil.json;

/**
 * Created by cleberzanella on 15/08/17.
 */
public class TimControllerProvider {

    private Service service;

    public TimControllerProvider(Service service) {
        this.service = service;
    }

    public void loadRoutes(){

        service.path("/tim", () -> {

            service.get("/consumption", TimController::getConsumption, json());

        });

    }

}
