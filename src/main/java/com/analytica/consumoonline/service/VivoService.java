package com.analytica.consumoonline.service;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.NicelyResynchronizingAjaxController;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.*;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by cleberzanella on 16/08/17.
 */
public class VivoService {

    public static void main(String... args){

        //new CelescService().getActualDebits("05592302908", 41336315);
        new VivoService().getActualDebits("cleber.zanella@gmail.com", "150360");

    }

    public List<Debit> getActualDebits(String cpfOrEmail, String password) {

        // Telefone: 47 3035-7067
        // https://legado.vivo.com.br/portal/site/meuvivo/meuvivohome?token=84b20cfe0b9abddbfeaaf743f08680a0&productId=4730357067

        // Outras consultas diretas:
        // https://meuvivo.vivo.com.br/meuvivo/publicServlets/ComboLinhasIntegracaoServlet?email=cleber.zanella%40gmail.com&documento=05592302908&familia=fixo&origem=meuvivo&_=1503268576774
        // https://meuvivo.vivo.com.br/meuvivo/publicServlets/ComboLinhasIntegracaoServlet?email=cleber.zanella%40gmail.com&documento=05592302908&familia=internet&origem=meuvivo&_=1503268576776

        final WebClient webClient = new WebClient();

        try {
            webClient.getOptions().setCssEnabled(true);
            webClient.getOptions().setJavaScriptEnabled(true);
            webClient.getOptions().setThrowExceptionOnScriptError(false);
            webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);

            HtmlPage page = webClient.getPage("http://www.vivo.com.br");

            ((HtmlTextInput) page.getElementsByName("cpfOuEmail").get(0)).type(cpfOrEmail);
            ((HtmlTextInput) page.getElementsByName("senhaText").get(0)).setValueAttribute(password);

            HtmlPage page2 = ((HtmlButtonInput)page.getElementsByName("btnEntrar").get(0)).click();

            HtmlDivision errorSpan = page2.getFirstByXPath("//div[@class='txt_msn_erro']");

            if(errorSpan != null){
                return Collections.emptyList();
            }

//             HtmlPage page = webClient.getPage("https://meuvivo.vivo.com.br/meuvivo/appmanager/portal/internet");
//
//            ((HtmlTextInput) page.getElementsByName("cpfOuEmail").get(0)).type(cpfOrEmail);
//            ((HtmlPasswordInput) page.getElementsById("passwordId_we").get(0)).type(password);
//
//            HtmlPage page2 = ((HtmlAnchor)page.getElementsById("btn-entrar-login-we").get(0)).click();
//
//            HtmlDivision errorSpan = page2.getFirstByXPath("//div[@class='loginPF_middlee_erro']");
//
//            if(errorSpan != null){
//                return Collections.emptyList();
//            }



            HtmlTableRow dataRow = page2.getFirstByXPath("//tr[@id='lineId0']");

            Debit debit = new Debit();

            debit.month = Integer.parseInt(dataRow.getCell(0).asText());
            debit.invoiceNumber = dataRow.getCell(2).asText().trim();
            debit.price = BigDecimal.valueOf(Double.parseDouble(dataRow.getCell(3).asText()));

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/M/yyyy");
            String dateStr = dataRow.getCell(1).asText();
            dateStr = dateStr.replace(" ", "");
            LocalDate dateTime = LocalDate.parse(dateStr, formatter);

            debit.paymentDay = dateTime;

            List<Debit> results = new ArrayList<>();
            results.add(debit);

            return results;

        } catch(IOException exc){
            exc.printStackTrace();
        } finally {
            webClient.close();
        }

        return null;
    }

    public static class Debit {
        public int month;
        public LocalDate paymentDay;
        public BigDecimal price;
        public String invoiceNumber;
    }

}
