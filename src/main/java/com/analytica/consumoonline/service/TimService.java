package com.analytica.consumoonline.service;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.*;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by cleberzanella on 15/08/17.
 */
public class TimService {

    public static void main(String... args){

        //new CelescService().getActualDebits("05592302908", 41336315);
        new TimService().getActualDebits("47999752047", 3632);

    }

    public List<ConsumptionItem> getActualDebits(String loginNumber, int password) {

        final WebClient webClient = new WebClient();

        try {
            webClient.getOptions().setCssEnabled(true);
            webClient.getOptions().setJavaScriptEnabled(true);
            webClient.getOptions().setThrowExceptionOnScriptError(false);
            webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
            //webClient.setAjaxController(new NicelyResynchronizingAjaxController());

            HtmlPage page = webClient.getPage("https://meutim.tim.com.br/menu/meus-creditos/detalhamento-de-consumo");

            ((HtmlTelInput) page.getElementsByName("loginField").get(0)).setValueAttribute(loginNumber);
            ((HtmlPasswordInput) page.getElementsByName("senha").get(0)).type(Integer.toString(password));

            HtmlPage page2 = ((HtmlButton)page.getFirstByXPath("//button[@type = 'submit']")).click();

            HtmlSpan errorSpan = page2.getFirstByXPath("//span[@class='msg-erro']");

            if(errorSpan != null && ! errorSpan.asText().trim().isEmpty()){

                return Collections.emptyList();
            }

            ((HtmlSelect) page2.getElementByName("periodo")).getOptionByValue(Integer.toString(7)).click();

            page2.getElementByName("showFree").click();
            page2.getElementById("todosServicos").click();

            HtmlPage reportPage = ((HtmlButton) page2.getFirstByXPath("//button[@type = 'submit']")).click();

            HtmlTable consumptionTable = reportPage.getFirstByXPath("//table[@class='tabela_consumo']");

            if(consumptionTable == null){
                return Collections.emptyList();
            }

            List<ConsumptionItem> consumption = new ArrayList<>();

            for(HtmlTableRow row : consumptionTable.getRows()){

                if("DATA".equals(row.getCell(0).asText())){
                    continue;
                }

                ConsumptionItem consumptionItem = new ConsumptionItem();

                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
                String dateStr = row.getCell(0).asText();
                dateStr = dateStr.replace(" ", "");
                LocalDate date = LocalDate.parse(dateStr, formatter);

                consumptionItem.date = date;


                DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");
                String timeStr = row.getCell(1).asText();
                dateStr = timeStr.replace(" ", "");
                LocalTime time = LocalTime.parse(timeStr, timeFormatter);

                consumptionItem.time = time;

                consumptionItem.history = row.getCell(2).asText();
                consumptionItem.originArea = row.getCell(3).asText();
                consumptionItem.destinNumber = row.getCell(4).asText();
                consumptionItem.destin = row.getCell(5).asText();
                consumptionItem.realized = row.getCell(6).asText();
                consumptionItem.type = row.getCell(7).asText();

                consumptionItem.price = BigDecimal.valueOf(Double.parseDouble(row.getCell(8).asText().replace(',','.')));

                // sanitize
                if("-".equals(consumptionItem.originArea)){
                    consumptionItem.originArea = null;
                }
                if("-".equals(consumptionItem.destin)){
                    consumptionItem.destin = null;
                }
                if(consumptionItem.type.length() == 0){
                    consumptionItem.type = null;
                }
                if(consumptionItem.destinNumber.length() == 0){
                    consumptionItem.destinNumber = null;
                }
                if(consumptionItem.realized.length() == 0){
                    consumptionItem.realized = null;
                }

                consumption.add(consumptionItem);
            }

            // logoff
            HtmlAnchor closeAnchor = reportPage.getFirstByXPath("//a[@title='Sair']");

            if(closeAnchor != null){
                HtmlPage closePage = closeAnchor.click();
                HtmlButton closeYes = closeAnchor.getFirstByXPath("//button[@class='bt-confirma-saida']");

                if(closeYes != null){
                    closeYes.click();
                }

            }

            return consumption;

        } catch(IOException exc){
            exc.printStackTrace();
        } finally {
            webClient.close();
        }

        return null;
    }

    public class ConsumptionItem {
        public LocalDate date;
        public LocalTime time;
        public String history;
        public String originArea;
        public String destin;
        public String destinNumber;
        public String realized;
        public String type;
        public BigDecimal price;
    }

}
