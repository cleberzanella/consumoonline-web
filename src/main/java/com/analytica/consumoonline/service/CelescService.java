package com.analytica.consumoonline.service;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.*;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by cleberzanella on 15/08/17.
 */
public class CelescService {


    public static void main(String... args){

        new CelescService().getActualDebits("05592302908", 41336315);
        //new CelescService().getActualDebits("055923", 41336);

    }

    public List<Debit> getActualDebits(String cpfCnpj, int consumptionUnit) {

        final WebClient webClient = new WebClient();

        try {
            webClient.getOptions().setCssEnabled(false);
            webClient.getOptions().setJavaScriptEnabled(false);

            HtmlPage page = webClient.getPage("http://agenciaweb.celesc.com.br:8080/AgenciaWeb/autenticar/loginUC.do");

            ((HtmlTextInput) page.getElementsByName("numDoc").get(0)).type(cpfCnpj);
            ((HtmlTextInput) page.getElementsByName("codUnCons").get(0)).type(Integer.toString(consumptionUnit));

            HtmlPage page2 = ((HtmlInput)page.getFirstByXPath("//input[@type = 'submit']")).click();

            if(page2.getElementById("avisoErro") != null){
                return Collections.emptyList();
            }

            HtmlTableRow dataRow = page2.getFirstByXPath("//table/tbody/tr[@class='fundoLinha2Tabela']");

            Debit debit = new Debit();

            debit.month = Integer.parseInt(dataRow.getCell(0).asText());
            debit.invoiceNumber = dataRow.getCell(2).asText().trim();
            debit.price = BigDecimal.valueOf(Double.parseDouble(dataRow.getCell(3).asText()));

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/M/yyyy");
            String dateStr = dataRow.getCell(1).asText();
            dateStr = dateStr.replace(" ", "");
            LocalDate dateTime = LocalDate.parse(dateStr, formatter);

            debit.paymentDay = dateTime;

            List<Debit> results = new ArrayList<>();
            results.add(debit);

            return results;

        } catch(IOException exc){
            exc.printStackTrace();
        } finally {
            webClient.close();
        }

        return null;
    }

    public static class Debit {
        public int month;
        public LocalDate paymentDay;
        public BigDecimal price;
        public String invoiceNumber;
    }

}
