package com.analytica.consumoonline.core.serialization;

import com.google.gson.Gson;
import spark.ResponseTransformer;

/**
 * Created by cleberzanella on 14/08/17.
 */
public class ResponseUtil {

    public static String toJson(Object object) {
        return new Gson().toJson(object);
    }

    public static ResponseTransformer json() {
        return ResponseUtil::toJson;
    }

}
