package com.analytica.consumoonline.setup;

//import io.undertow.Undertow;
//import io.undertow.servlet.Servlets;
//import io.undertow.servlet.api.DeploymentInfo;
//import io.undertow.servlet.api.DeploymentManager;
//import io.undertow.servlet.api.FilterInfo;
//import spark.servlet.SparkFilter;
//
//import javax.servlet.DispatcherType;

import static spark.Spark.port;


/**
 * Created by cleberzanella on 13/08/17.
 */
public class Main {

    private static final String HTTP_PORT = "PORT";

    // http://localhost:4567/myapp/myservlet
    public static void main(String... args) throws Exception {

        // How to: https://stackoverflow.com/questions/35269763/is-there-a-way-to-use-the-servlet-api-with-undertow
//        FilterInfo filter = Servlets.filter(SparkFilter.class);
//        filter.addInitParam(SparkFilter.APPLICATION_CLASS_PARAM, Application.class.getName());
//
//        DeploymentInfo servletBuilder = Servlets.deployment().setClassLoader(Main.class.getClassLoader())
//                .setDeploymentName("myapp").setContextPath("/")
//                .addFilter(filter)
//                .addFilterUrlMapping(SparkFilter.class.getSimpleName(), "/*", DispatcherType.REQUEST);
//
//        DeploymentManager manager = Servlets.defaultContainer().addDeployment(servletBuilder);
//        manager.deploy();
//
//        Undertow server = Undertow.builder().addHttpListener(getAssignedPort(), "localhost").setHandler(manager.start()).build();
//        server.start();

        int port = getAssignedPort();
        new Application(port).init();

    }

    static int getAssignedPort() {
        ProcessBuilder processBuilder = new ProcessBuilder();

        if (processBuilder.environment().get(HTTP_PORT) != null) {
            return Integer.parseInt(processBuilder.environment().get(HTTP_PORT));
        }

        return 8080; //return default port if port isn't set (i.e. on localhost)
    }
}
