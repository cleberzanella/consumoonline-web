package com.analytica.consumoonline.setup;

import com.analytica.consumoonline.controllerprovider.CelescControllerProvider;
import com.analytica.consumoonline.controllerprovider.TimControllerProvider;
import spark.Service;
import spark.servlet.SparkApplication;

/**
 * Created by cleberzanella on 13/08/17.
 */
public class Application   implements SparkApplication {

    private int port;

    public Application(int port) {
        this.port = port;
    }

    public void init() {

        setupRoutes();

        //setupPersistence();

    }

    private void setupRoutes() {

        Service service = Service.ignite();

        service.port(port);

//        service.before((request, response) -> {
//            service.halt(401, "You are welcome here");
//        });

        service.get("/", (request, response) -> "hello!!");

        //new CustomerControllerProvider(service).loadRoutes();
        new CelescControllerProvider(service).loadRoutes();
        new TimControllerProvider(service).loadRoutes();
    }

    private void setupPersistence() {
        //EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("JPATest");
        //Db.create(entityManagerFactory);
    }

}
