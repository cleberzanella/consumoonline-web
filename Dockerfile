FROM java:8-jdk

# Install gradle
RUN apt-get update
#RUN apt-get -y install gradle


# Prepare by downloading dependencies
#ADD build.gradle /code/build.gradle

# Adding source to compile
#ADD src /code/src

#WORKDIR /code
#RUN gradle build -x test
#RUN gradle allInOneJar

# Heroku Procfile
ADD Procfile /app/Procfile
ADD build/libs/server.jar /app/server.jar

WORKDIR /app

# Run the image as a non-root user
RUN useradd -d /code -m myuser
USER myuser

CMD ["java", "-jar", "server.jar"]